#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
import json
import sys

if len(sys.argv) != 6:
    print("Usage: {} <hostname> <api_username> <api_password> <lun_uuid> <lun_name>".format(sys.argv[0]))
    sys.exit(3)

hostname = sys.argv[1]
api_username = sys.argv[2]
api_password = sys.argv[3]
lun_uuid = sys.argv[4]
lun_name = sys.argv[5]

# Ejecutar el comando Centreon Plugin
centreon_command = "/usr/lib/centreon/plugins/centreon_netapp_ontap_restapi.pl --plugin storage::netapp::ontap::restapi::plugin --mode luns --hostname {} --api-username $

try:
    centreon_output = subprocess.check_output(centreon_command, shell=True)

    if "OK" in centreon_output:
        exit_status = 0
    elif "WARNING" in centreon_output:
        exit_status = 1
    elif "CRITICAL" in centreon_output:
        exit_status = 2
    else:
        exit_status = 3

except subprocess.CalledProcessError as e:
    print("UNKNOWN: Error executing Centreon Plugin command: {}".format(e))
    sys.exit(3)

# Ejecutar el comando curl para obtener la métrica de latencia total
curl_command = 'curl -s -k -u "{}:{}" "https://{}/api/storage/luns/{}?fields=metric.latency"'.format(api_username, api_password, hostname, lun_uuid)

try:
    curl_output = subprocess.check_output(curl_command, shell=True)
    latency_data = json.loads(curl_output)

    latency_total = latency_data['metric']['latency']['total']
    latency_read = latency_data['metric']['latency']['read']
    latency_write = latency_data['metric']['latency']['write']
    latency_other = latency_data['metric']['latency']['other']

    latency_line = "{}: Lun '{}' state: online [container state: online] | Latencia total: {} ms, Latencia lectura: {} ms, Latencia escritura: {} ms, Otra latencia: {} m$
        "OK" if exit_status == 0 else "CRITICAL", lun_name, latency_total, latency_read, latency_write, latency_other,
        latency_total, latency_read, latency_write, latency_other)

    print(latency_line)

except subprocess.CalledProcessError as e:
    print("UNKNOWN: Error executing curl command: {}".format(e))
    sys.exit(3)

except (IndexError, ValueError, KeyError) as e:
    print("UNKNOWN: Error extracting latency metric: {}".format(e))
    sys.exit(3)

# Salir con el estado obtenido del comando Centreon
sys.exit(exit_status)
